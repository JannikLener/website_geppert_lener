﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public class Message
    {
        public string Header { get; set; }
        public string AdditionalHeader { get; set; }
        public string MessageText { get; set; }
        public string Solution { get; set; }

        public Message() : this("", "", "", "") { }
        public Message(string head, string mess) : this(head, "", mess, "") { }
        public Message(string head1, string head2, string mess, string sol)
        {
            this.Header = head1;
            this.AdditionalHeader = head2;
            this.MessageText = mess;
            this.Solution = sol;
        }
    }
}