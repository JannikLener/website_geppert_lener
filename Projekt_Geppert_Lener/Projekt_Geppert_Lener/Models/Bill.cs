﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Projekt_Geppert_Lener.Models;

namespace Projekt_Geppert_Lener.Models
{
    public class Bill
    {
        public List<BasketItem> BasketContent = new List<BasketItem>();
        public User Buyer { get; set; }
        

        public Bill() : this(null, null) { }
        public Bill(List<BasketItem> basketcontent, User buyer)
        {
            this.BasketContent = basketcontent;
            this.Buyer = buyer;
        }

        public decimal SummPrice()
        {
            decimal summPrice = 0.0m;
            foreach (BasketItem b in BasketContent)
            {
                summPrice += (b.Article.Price * b.Basket.Amount);
            }
            return summPrice;
        }

        public override string ToString()
        {
            string s = "Vielen Dank für deine Bestellung " + Buyer.Firstname + " " + Buyer.Lastname + "\n\n";
            foreach (BasketItem b in BasketContent)
            {
                s += b.Article.Name + " " + b.Article.Price + " " + b.Article.Amount + " " + b.Article.Amount * b.Article.Price + "\n";
            }
            return s;
        }

    }
}