﻿using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.IO;

namespace Projekt_Geppert_Lener.Models.Database
{
    public class RepArticle : IRepArticle
    {
        private string connectionString = "Server=localhost; Database=userdatabase; Uid=root; Pwd=";
        private MySqlConnection connection;

        public void Open()
        {
            if (this.connection == null)
            {
                this.connection = new MySqlConnection(this.connectionString);
            }
            if (this.connection.State != ConnectionState.Open)
            {
                this.connection.Open();
            }
            if (this.connection.State == ConnectionState.Open) { }
        }
        public void Close()
        {
            if ((this.connection != null) && (this.connection.State == ConnectionState.Open))
            {
                this.connection.Close();
            }
        }

        public List<Article> GetAllArticles()
        {
            List<Article> articles = new List<Article>();
            try
            {
                MySqlCommand cmdAllarticles = this.connection.CreateCommand();
                cmdAllarticles.CommandText = "select * from articles";
                using (MySqlDataReader reader = cmdAllarticles.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        articles.Add(new Article()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            Seller = Convert.ToString(reader["seller"]),
                            Price = Convert.ToDecimal(reader["price"]),
                            ArticleType = (articleType)Convert.ToInt32(reader["articleType"]),
                            Amount = Convert.ToInt32(reader["amount"]),
                            IsOfficial = Convert.ToBoolean(reader["isOfficial"]),
                            ImagePath = Convert.ToString(reader["img"])
                            
                        });
                    }
                }
                return articles.Count == 0 ? null : articles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Article GetOneArticle(int idToFind)
        {
            try
            {
                MySqlCommand cmdFind = this.connection.CreateCommand();
                cmdFind.CommandText = "select * from articles where id=@ID";
                cmdFind.Parameters.AddWithValue("ID", idToFind);

                using (MySqlDataReader reader = cmdFind.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new Article()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            Seller = Convert.ToString(reader["seller"]),
                            Price = Convert.ToDecimal(reader["price"]),
                            ArticleType = (articleType)Convert.ToInt32(reader["articleType"]),
                            Amount = Convert.ToInt32(reader["amount"]),
                            IsOfficial = Convert.ToBoolean(reader["isOfficial"]),
                            ImagePath = Convert.ToString(reader["img"])
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AddArticle(Article articleToAdd)
        {
            if (articleToAdd == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsert = this.connection.CreateCommand();
                cmdInsert.CommandText = "insert into articles values(null,@name, @price, @seller,@articleType,@amount, @isOfficial, @img);";
                cmdInsert.Parameters.AddWithValue("name", articleToAdd.Name);
                cmdInsert.Parameters.AddWithValue("price", articleToAdd.Price);
                cmdInsert.Parameters.AddWithValue("seller", articleToAdd.Seller);
                cmdInsert.Parameters.AddWithValue("articleType", articleToAdd.ArticleType);
                cmdInsert.Parameters.AddWithValue("amount", articleToAdd.Amount);
                cmdInsert.Parameters.AddWithValue("isOfficial", articleToAdd.IsOfficial);
                cmdInsert.Parameters.AddWithValue("img", articleToAdd.ImagePath);

                return cmdInsert.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteArticle(int idToDelete)
        {
            try
            {
                MySqlCommand cmdDelete = this.connection.CreateCommand();
                cmdDelete.CommandText = "Delete from articles where id=@ID";
                cmdDelete.Parameters.AddWithValue("ID", idToDelete);

                return cmdDelete.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ChangeAmount(int idToChange, int amount)
        {
            try
            {
                if ((GetOneArticle(idToChange) != null) && (amount >= 0))
                {
                    MySqlCommand cmdUpdate = this.connection.CreateCommand();
                    cmdUpdate.CommandText = "update articles set amount=@AMOUNT where id=@ID";
                    cmdUpdate.Parameters.AddWithValue("ID", idToChange);
                    cmdUpdate.Parameters.AddWithValue("AMOUNT", amount);

                    return cmdUpdate.ExecuteNonQuery() == 1;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}