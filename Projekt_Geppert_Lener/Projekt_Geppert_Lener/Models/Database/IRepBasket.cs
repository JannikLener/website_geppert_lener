﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geppert_Lener.Models.Database
{
    interface IRepBasket
    {
        void Open();
        void Close();

        Basket GetOneItem(int idu, int ida);
        List<Basket> GetAllItems(int userID);
        bool AddArticleToBasket(int articleID, int userID, int amount);
        bool RemoveArticlefromBasket(int articleID, int userID);
        void ClearBasket(int userID);
        Article GetOneArticleForBasket(int idToFind);
        List<BasketItem> GetAllItemsAndArticles(int userID);
    }
}
