﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models.Database
{
    public class RepBasket : IRepBasket
    {
        private string connectionString = "Server=localhost; Database=userdatabase; Uid=root; Pwd=";
        private MySqlConnection connection;

        public void Open()
        {
            if (this.connection == null)
            {
                this.connection = new MySqlConnection(this.connectionString);
            }
            if (this.connection.State != ConnectionState.Open)
            {
                this.connection.Open();
            }
            if (this.connection.State == ConnectionState.Open) { }
        }
        public void Close()
        {
            if ((this.connection != null) && (this.connection.State == ConnectionState.Open))
            {
                this.connection.Close();
            }
        }

        public List<Basket> GetAllItems(int userID)
        {
            List<Basket> basket = new List<Basket>();
            try
            {
                MySqlCommand cmdAllItems = this.connection.CreateCommand();
                cmdAllItems.CommandText = "select * from articleusers where userid=@ID";
                cmdAllItems.Parameters.AddWithValue("ID", userID);

                using (MySqlDataReader reader = cmdAllItems.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        basket.Add(new Basket()
                        {
                            UserID = Convert.ToInt32(reader["userid"]),
                            ArticleID = Convert.ToInt32(reader["articleid"]),
                            Amount = Convert.ToInt32(reader["amount"]),
                        });
                    }
                }
                return basket.Count == 0 ? null : basket;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<BasketItem> GetAllItemsAndArticles(int userID)
        {
            List<BasketItem> bi = new List<BasketItem>();
            List<Basket> b = new List<Basket>();
            b = GetAllItems(userID);
            if (b != null)
            {
                foreach (Basket bask in b)
                {
                    bi.Add(new BasketItem(this.GetOneArticleForBasket(bask.ArticleID), bask));
                }
            }
            return bi;
        }
        public Basket GetOneItem(int idu, int ida)
        {
            try
            {
                MySqlCommand cmdFind = this.connection.CreateCommand();
                cmdFind.CommandText = "select * from articleusers where userid=@IDU and articleid=@IDA";
                cmdFind.Parameters.AddWithValue("IDU", idu);
                cmdFind.Parameters.AddWithValue("IDA", ida);

                using (MySqlDataReader reader = cmdFind.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new Basket()
                        {
                            UserID = Convert.ToInt32(reader["userid"]),
                            ArticleID = Convert.ToInt32(reader["articleid"]),
                            Amount = Convert.ToInt32(reader["amount"]),
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool AddArticleToBasket(int articleID, int userID, int amount)
        {
            try
            {
                if ((GetOneItem(userID, articleID) != null)&&(amount>0))
                {
                    MySqlCommand cmdUpdate = this.connection.CreateCommand();
                    cmdUpdate.CommandText = "update articleusers set amount=@AMOUNT where userid=@IDU and articleid=@IDA";
                    cmdUpdate.Parameters.AddWithValue("IDU", userID);
                    cmdUpdate.Parameters.AddWithValue("IDA", articleID);
                    cmdUpdate.Parameters.AddWithValue("AMOUNT", amount);

                    return cmdUpdate.ExecuteNonQuery() == 1;
                }
                else if(((GetOneItem(userID, articleID) != null) && (amount < 1)))
                {
                    return RemoveArticlefromBasket(articleID, userID);
                }
                else
                {
                    MySqlCommand cmdInsert = this.connection.CreateCommand();
                    cmdInsert.CommandText = "insert into articleusers values(@article, @user, @amount);";
                    cmdInsert.Parameters.AddWithValue("article", articleID);
                    cmdInsert.Parameters.AddWithValue("user", userID);
                    cmdInsert.Parameters.AddWithValue("amount", amount);

                    return cmdInsert.ExecuteNonQuery() == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Article GetOneArticleForBasket(int idToFind)
        {
            try
            {
                MySqlCommand cmdFind = this.connection.CreateCommand();
                cmdFind.CommandText = "select * from articles where id=@ID";
                cmdFind.Parameters.AddWithValue("ID", idToFind);

                using (MySqlDataReader reader = cmdFind.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new Article()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            Seller = Convert.ToString(reader["seller"]),
                            Price = Convert.ToDecimal(reader["price"]),
                            ArticleType = (articleType)Convert.ToInt32(reader["articleType"]),
                            Amount = Convert.ToInt32(reader["amount"])
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool RemoveArticlefromBasket(int articleID, int userID)
        {
            try
            {
                MySqlCommand cmdDelete = this.connection.CreateCommand();
                cmdDelete.CommandText = "Delete from articleusers where userid=@IDU and articleid=@IDA";
                cmdDelete.Parameters.AddWithValue("IDU", userID);
                cmdDelete.Parameters.AddWithValue("IDA", articleID);

                return cmdDelete.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
         }

        public void ClearBasket(int userID)
        {
            try
            {
                MySqlCommand cmdClear = this.connection.CreateCommand();
                cmdClear.CommandText = "Delete from articleusers where userid=@IDU";
                cmdClear.Parameters.AddWithValue("IDU", userID);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}