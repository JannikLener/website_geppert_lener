﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models.Database
{
    public class RepChat : IRepChat
    {
        private string connectionString = "Server=localhost; Database=userdatabase; Uid=root; Pwd=";
        private MySqlConnection connection;

        public void Open()
        {
            if (this.connection == null)
            {
                this.connection = new MySqlConnection(this.connectionString);
            }
            if (this.connection.State != ConnectionState.Open)
            {
                this.connection.Open();
            }
            if (this.connection.State == ConnectionState.Open) { }
        }
        public void Close()
        {
            if ((this.connection != null) && (this.connection.State == ConnectionState.Open))
            {
                this.connection.Close();
            }
        }

        public List<Chat> AllMessagesToMe(int recievID)
        {
            List<Chat> chat = new List<Chat>();
            try
            {
                MySqlCommand cmdMessagesToMe = this.connection.CreateCommand();
                cmdMessagesToMe.CommandText = "select * from chat where reciever = @reciever";
                cmdMessagesToMe.Parameters.AddWithValue("reciever", recievID);
                using (MySqlDataReader reader = cmdMessagesToMe.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        chat.Add(new Chat()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Sender = Convert.ToInt32(reader["sender"]),
                            Reciever = Convert.ToInt32(reader["reciever"]),
                            Subject = Convert.ToString(reader["subject"]),
                            Text = Convert.ToString(reader["text"])
                        });
                    }
                }
                return chat.Count == 0 ? null : chat;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Chat> AllMessagesFromMe(int sendID)
        {
            List<Chat> chat = new List<Chat>();
            try
            {
                MySqlCommand cmdMessagesFromMe = this.connection.CreateCommand();
                cmdMessagesFromMe.CommandText = "select * from chat where sender = @sender";
                cmdMessagesFromMe.Parameters.AddWithValue("sender", sendID);
                using (MySqlDataReader reader = cmdMessagesFromMe.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        chat.Add(new Chat()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Sender = Convert.ToInt32(reader["sender"]),
                            Reciever = Convert.ToInt32(reader["reciever"]),
                            Subject = Convert.ToString(reader["subject"]),
                            Text = Convert.ToString(reader["text"])
                        });
                    }
                }
                return chat.Count == 0 ? null : chat;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Chat GetOneMessage(int chatID)
        {
            try
            {
                MySqlCommand cmdOneMessages = this.connection.CreateCommand();
                cmdOneMessages.CommandText = "select * from chat where id = @id";
                cmdOneMessages.Parameters.AddWithValue("id", chatID);
                using (MySqlDataReader reader = cmdOneMessages.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new Chat()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Sender = Convert.ToInt32(reader["id"]),
                            Reciever = Convert.ToInt32(reader["id"]),
                            Subject = Convert.ToString(reader["name"]),
                            Text = Convert.ToString(reader["name"])
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool NewMessage(Chat chat)
        {
            if (chat == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdAdd = this.connection.CreateCommand();
                cmdAdd.CommandText = "insert into chat values(null,@sender, @reciever, @subject,@text);";
                cmdAdd.Parameters.AddWithValue("sender", chat.Sender);
                cmdAdd.Parameters.AddWithValue("reciever", chat.Reciever);
                cmdAdd.Parameters.AddWithValue("subject", chat.Subject);
                cmdAdd.Parameters.AddWithValue("text", chat.Text);
                return cmdAdd.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int NameToID(string user)
        {
            try
            {
                MySqlCommand cdmNameToID = this.connection.CreateCommand();
                cdmNameToID.CommandText = "select * from users where username = @username";
                cdmNameToID.Parameters.AddWithValue("username", user);
                using (MySqlDataReader reader = cdmNameToID.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (int)reader["id"];
                    }
                    else
                    {
                        return -1;
                    }
                }
            }           
            catch (Exception)
            {
                throw;
            }
        }
        public string IDToName(int id)
        {
            try
            {
                MySqlCommand cdmNameToID = this.connection.CreateCommand();
                cdmNameToID.CommandText = "select * from users where id = @id";
                cdmNameToID.Parameters.AddWithValue("id", id);
                using (MySqlDataReader reader = cdmNameToID.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (string)reader["username"];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}