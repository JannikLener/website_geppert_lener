﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geppert_Lener.Models.Database
{
    interface IRepUser
    {
        void Open();
        void Close();

        User GetUser(int idToFind);
        List<User> GetAllUsers();
        bool Deactivate(int idToDeactivate);
        bool ChangeUser(int idToChange, User newUser);
        User Authenticate(string eMailOrUsername, string passwrd);
        bool Register(User personToRegister);
    }
}
