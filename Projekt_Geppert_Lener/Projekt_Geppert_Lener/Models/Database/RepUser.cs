﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models.Database
{
    class RepUser : IRepUser
    {

        private string connectionString = "Server=localhost; Database=userdatabase; Uid=root; Pwd=";
        private MySqlConnection connection;

        public void Open()
        {
            if (this.connection == null)
            {
                this.connection = new MySqlConnection(this.connectionString);
            }
            if (this.connection.State != ConnectionState.Open)
            {
                this.connection.Open();
            }
            if (this.connection.State == ConnectionState.Open) {}
        }
        public void Close()
        {
            if ((this.connection != null) && (this.connection.State == ConnectionState.Open))
            {
                this.connection.Close();
            }
        }

        public List<User> GetAllUsers()
        {
            List<User> people = new List<User>();
            try
            {
                MySqlCommand cmdAllpeople = this.connection.CreateCommand();
                cmdAllpeople.CommandText = "select * from users";
                using (MySqlDataReader reader = cmdAllpeople.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        people.Add(new User()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            Gender = (gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                            IsAdmin = Convert.ToBoolean(reader["isAdmin"]),
                            Password = Convert.ToString(reader["passwrd"]),
                        });
                    }
                }
                return people.Count == 0 ? null : people;
                /*
                 if (people.Count == 0)
                 {
                 return null;
                 }
                 else{
                 return people;
                 }
                 */
            }
            catch (Exception)
            {
                throw;
            }
        }
        public User GetUser(int idToFind)
        {
            try
            {
                MySqlCommand cmdFind = this.connection.CreateCommand();
                cmdFind.CommandText = "select * from users where id=@ID";
                cmdFind.Parameters.AddWithValue("ID", idToFind);

                using (MySqlDataReader reader = cmdFind.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new User()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            Gender = (gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                            IsAdmin = Convert.ToBoolean(reader["isAdmin"]),
                            Password = Convert.ToString(reader["passwrd"]),
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public User Authenticate(string eMailOrUsername, string passwrd)
        {
            try
            {
                MySqlCommand cmdLogin = this.connection.CreateCommand();
                cmdLogin.CommandText = "select * from users where (username = @usernameOREmail Or email = @usernameOrEmail) And passwrd = sha1(@passw)";
                cmdLogin.Parameters.AddWithValue("usernameOREmail", eMailOrUsername);
                cmdLogin.Parameters.AddWithValue("passw", passwrd);

                using (MySqlDataReader reader = cmdLogin.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new User()
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            Gender = (gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                            IsAdmin = Convert.ToBoolean(reader["isAdmin"]),
                            Password = Convert.ToString(reader["passwrd"]),
                        };
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return null;
        }
        public bool Register(User personToRegister)
        {
            if (personToRegister == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdInsert = this.connection.CreateCommand();
                cmdInsert.CommandText = "insert into users values(null,@firstname, @lastname, @gender,@email,@username,sha1(@passwrd),@isAdmin);";
                cmdInsert.Parameters.AddWithValue("firstname", personToRegister.Firstname);
                cmdInsert.Parameters.AddWithValue("lastname", personToRegister.Lastname);
                cmdInsert.Parameters.AddWithValue("gender", personToRegister.Gender);
                cmdInsert.Parameters.AddWithValue("email", personToRegister.EMail);
                cmdInsert.Parameters.AddWithValue("username", personToRegister.Username);
                cmdInsert.Parameters.AddWithValue("passwrd", personToRegister.Password);
                cmdInsert.Parameters.AddWithValue("isAdmin", personToRegister.IsAdmin);

                return cmdInsert.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool ChangeUser(int idToChange, User newUser)
        {
            if (newUser == null)
            {
                return false;
            }
            try
            {
                MySqlCommand cmdChange = this.connection.CreateCommand();

                cmdChange.CommandText = "update users set firstname=@firstname, lastname=@lastname, gender=@gender, email=@email, username=@username, passwrd=sha1(@passwrd), isAdmin=@isAdmin where id=@id;";
                cmdChange.Parameters.AddWithValue("firstname", newUser.Firstname);
                cmdChange.Parameters.AddWithValue("lastname", newUser.Lastname);
                cmdChange.Parameters.AddWithValue("gender", newUser.Gender);
                cmdChange.Parameters.AddWithValue("email", newUser.EMail);
                cmdChange.Parameters.AddWithValue("username", newUser.Username);
                cmdChange.Parameters.AddWithValue("passwrd", newUser.Password);
                cmdChange.Parameters.AddWithValue("isAdmin", newUser.IsAdmin);
                cmdChange.Parameters.AddWithValue("id", idToChange);

                return cmdChange.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Deactivate(int idToDeactivate)
        {
            throw new NotImplementedException();
        }
    }
}
