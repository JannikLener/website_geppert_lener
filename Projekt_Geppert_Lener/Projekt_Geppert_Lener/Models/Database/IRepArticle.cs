﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geppert_Lener.Models.Database
{
    interface IRepArticle
    {
        void Open();
        void Close();

        Article GetOneArticle(int idToFind);
        List<Article> GetAllArticles();
        bool AddArticle(Article articleToAdd);
        bool DeleteArticle(int idToDelete);
        bool ChangeAmount(int idToChange, int amount);
    }
}
