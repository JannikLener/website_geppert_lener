﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geppert_Lener.Models.Database
{
    interface IRepChat
    {
        void Open();
        void Close();

        List<Chat> AllMessagesToMe(int recievID);
        List<Chat> AllMessagesFromMe(int sendID);
        Chat GetOneMessage(int chatID);
        bool NewMessage(Chat chat);
        int NameToID(string user);
        string IDToName(int id);
    }
}
