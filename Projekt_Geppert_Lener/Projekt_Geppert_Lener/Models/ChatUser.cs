﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public class ChatUser
    {
        public Chat Chat { get; set; }
        public string RecieverName { get; set; }

        public ChatUser () : this(new Chat(),"") { }
        public ChatUser(Chat c, string u)
        {
            this.Chat = c;
            this.RecieverName = u;
        }
    }
}