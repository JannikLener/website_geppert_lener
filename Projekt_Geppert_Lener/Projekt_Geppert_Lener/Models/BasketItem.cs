﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public class BasketItem
    {
        public Article Article { get; set; }
        public Basket Basket { get; set; }

        public BasketItem() : this(null, null) { }
        public BasketItem(Article a, Basket b)
        {
            this.Article = a;
            this.Basket = b;
        }

        
    }
}