﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public class Chat
    {
        public int ID { get; set; }
        public int Sender { get; set; }
        public int Reciever { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }

        public Chat() : this(-1, -1, -1, "", "") { }
        public Chat(int sender) : this(-1, sender, -1, "", "") { }
        public Chat(int id, int sender, int reciever, string subject, string text)
        {
            this.ID = id;
            this.Sender = sender;
            this.Reciever = reciever;
            this.Subject = subject;
            this.Text = text;
        }
    }
}