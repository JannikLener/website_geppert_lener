﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public enum gender
    {
        male, female, notSpecified
    }
    public class User
    {
        public int ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public gender Gender { get; set; }
        public string EMail { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public List<Article> Basket { get; set; }

        public User() : this(-1, "", "", gender.notSpecified, "", "", "", false, new List<Article>()) { }
        public User(int id, string firstname, string lastname, gender gender, string email, string username, string password, bool admin, List<Article>basket)
        {
            this.ID = id;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Gender = gender;
            this.EMail = email;
            this.Username = username;
            this.Password = password;
            this.IsAdmin = admin;
            this.Basket = basket;
        }
    }
}