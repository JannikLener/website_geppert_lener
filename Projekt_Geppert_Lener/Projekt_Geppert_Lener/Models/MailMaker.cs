﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;

namespace Projekt_Geppert_Lener.Models
{
    public class MailMaker
    {
        public string Absender { get; set;}

        public List<string> Empfänger { get; set;}

        public string Betreff
        {
            get;
            set;
        }

        public string Nachricht
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        public string Passwort
        {
            private get;
            set;
        }

        public string Servername
        {
            get;
            set;
        }

        public string Port
        {
            get;
            set;
        }


        public void Send()
        {
            MailMessage Email = new MailMessage();

            MailAddress Sender = new MailAddress(Absender);
            Email.From = Sender; // Absender einstellen

            // Empfänger hinzufügen
            foreach (string e in Empfänger)
            {
                Email.To.Add(e);
            }


            Email.Subject = Betreff; // Betreff hinzufügen

            Email.Body = Nachricht; // Nachrichtentext hinzufügen

            SmtpClient MailClient = new SmtpClient(Servername, int.Parse(Port)); // Postausgangsserver definieren

            string UserName = Username;
            string Password = Passwort;
            System.Net.NetworkCredential Credentials = new System.Net.NetworkCredential(UserName, Password);

            MailClient.Credentials = Credentials; // Anmeldeinformationen setzen

            MailClient.Send(Email); // Email senden
        }

    }
}