﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public enum articleType
    {
        mask, fur, smthgDifferent
    }

    public class Article
    {
        private decimal price;

        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                if(value >= 0.0m)
                {
                    this.price = value;
                }
            }
        }
        public string Seller { get; set; }
        public articleType ArticleType { get; set; }
        public int Amount { get; set; }
        public bool IsOfficial { get; set; }
        public string ImagePath { get; set; }
        public HttpPostedFileBase Img { get; set; }

        public Article():this (-1,"",0.0m,"",articleType.smthgDifferent,0, true,""/*,null*/){}
        public Article(int id, string name, decimal price, string seller, articleType aType, int amount, bool isofficial,string imgpath /*,HttpPostedFile img*/)
        {
            this.ID = id;
            this.Name = name;
            this.Price = price;
            this.Seller = seller;
            this.ArticleType = aType;
            this.Amount = amount;
            this.IsOfficial = isofficial;
            this.ImagePath = imgpath;
            //this.Img = Img;
        }
    }
}