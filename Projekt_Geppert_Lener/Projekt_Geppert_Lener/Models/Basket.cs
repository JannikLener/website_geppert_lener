﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekt_Geppert_Lener.Models
{
    public class Basket
    {
        private int amount;

        public int ArticleID { get; set; }
        public int UserID { get; set; }
        public int Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                if (value >= 0)
                {
                    this.amount = value;
                }
            }
        }

        public Basket() : this(-1,-1,0) { }
        public Basket(int aid, int uid, int am)
        {
            this.ArticleID = aid;
            this.UserID = uid;
            this.Amount = am;
        }
    }
}