﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Projekt_Geppert_Lener.Models;
using Projekt_Geppert_Lener.Models.Database;
using MySql.Data.MySqlClient;
using System.IO;

namespace Projekt_Geppert_Lener.Controllers
{
    public class ShopController : Controller
    {
        RepArticle repArticle = new RepArticle();

        // GET: Shop
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ArticleOverview()
        {
            List<Article> a;
            try
            {
                repArticle.Open();
                a = repArticle.GetAllArticles();
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
            }
            catch (Exception)
            {
                return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
            }
            finally
            {
                repArticle.Close();
            }
            if (a != null)
            {
                a = GetOfficial(a);
            }
            else
            {
                a = null;
            }
            return View(a);
        }

        public ActionResult ArticleDetailed(int id)
        {
            Article a;
            try
            {
                repArticle.Open();
                a = repArticle.GetOneArticle(id);
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
            }
            catch (Exception)
            {
                return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
            }
            finally
            {
                repArticle.Close();
            }
            return View(a);
        }

        [HttpGet]
        public ActionResult AddArticle()
        {
            User u = (User)Session["loggedInUser"];
            if (u.IsAdmin == true)
            {
                Article ar = new Article();
                return View(ar);
            }
            else
            {
                return View("Message", new Message("User Rechte", "Sie verfügen nicht über genug Rechte um auf diese Seite zu zugreifen."));
            }
        }

        [HttpPost]
        public ActionResult AddArticle(Article articleDataFromForm)
        {
            User u = (User)Session["loggedInUser"];
            if (u.IsAdmin == true)
            {
                if (articleDataFromForm != null)
                {
                    try
                    {
                        repArticle.Open();

                        CheckArticleData(articleDataFromForm);

                        if (ModelState.IsValid)
                        {
                            //Pfad für gespeicherte Datei am Server generieren
                            string path = Path.Combine(Server.MapPath("~/Media/shop/"),Path.GetFileName(articleDataFromForm.Img.FileName));
                           
                            // Vorsicht: falls Bild schon vorhanden ist: Guid

                            //Bild auf diesem Pfad speichern
                            articleDataFromForm.Img.SaveAs(path);
                            //Pfad in Objekt setzen, damit dieser in der Datenbank verfügbar ist
                            articleDataFromForm.ImagePath = "/Media/shop/" + Path.GetFileName(articleDataFromForm.Img.FileName);
                            repArticle.AddArticle(articleDataFromForm);
                            ViewBag.Message = "File uploaded successfully";
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repArticle.Close();
                    }
                    if (ModelState.IsValid)
                    {
                        return View("Message", new Message("Artikel hinzufügen", "Erfolgreich hinzugefügt"));
                    }
                    else
                    {
                        return View(articleDataFromForm);
                    }
                }
                else
                {
                    return RedirectToAction("AddArticle");
                }
            }
            else
            {
                return View("Message", new Message("User Rechte", "Sie verfügen nicht über genug Rechte um auf diese Seite zu zugreifen."));
            }
        }

        public ActionResult EditArticle()
        {
            User u = (User)Session["loggedInUser"];
            if (u.IsAdmin == true)
            {
                List<Article> a;
                try
                {
                    repArticle.Open();
                    a = repArticle.GetAllArticles();
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemiener Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repArticle.Close();
                }
                if (a != null)
                {
                    a = GetOfficial(a);
                }
                else
                {
                    a = null;
                }
                return View(a);
            }
            else
            {
                return View("Message", new Message("User Rechte", "Sie verfügen nicht über genug Rechte um auf diese Seite zu zugreifen."));
            }
        }

        public ActionResult ChangeAmount(int articleID, int amount)
        {
            bool wasChanged;
            User u = (User)Session["loggedInUser"];
            if (u.IsAdmin == true)
            {
                try
                {
                    repArticle.Open();
                    Article articleAmount = repArticle.GetOneArticle(articleID);
                    wasChanged = repArticle.ChangeAmount(articleID, articleAmount.Amount + amount);
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repArticle.Close();
                }
                if (wasChanged == true)
                {
                    return RedirectToAction("EditArticle");
                }
                else
                {
                    return View("Message", new Message("Unbekannter Fehler", "Der Artikelverfügbarkeit konnte nicht bearbeitet werden"));
                }
            }
            else
            {
                return View("Message", new Message("User Rechte", "Sie verfügen nicht über genug Rechte um auf diese Seite zu zugreifen."));
            }
        }

        public ActionResult DeleteArticle(int articleID)
        {
            bool wasDeleted;
            User u = (User)Session["loggedInUser"];
            if (u.IsAdmin == true)
            {
                try
                {
                    repArticle.Open();
                    wasDeleted = repArticle.DeleteArticle(articleID);
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repArticle.Close();
                }
                if (wasDeleted == true)
                {
                    return RedirectToAction("EditArticle");
                }
                else
                {
                    return View("Message", new Message("Unbekannter Fehler", "Der Artikel konnte nicht gelöscht werden"));
                }
            }
            else
            {
                return View("Message", new Message("User Rechte", "Sie verfügen nicht über genug Rechte um auf diese Seite zu zugreifen."));
            }
        }

        public List<Article> GetOfficial(List<Article> allArticles)
        {
            List<Article> officialArticles = new List<Article>();
            foreach (Article a in allArticles)
            {
                if (a.IsOfficial == true)
                {
                    officialArticles.Add(a);
                }
            }
            if (officialArticles.Count > 0)
            {
                return officialArticles;
            }
            else
            {
                return null;
            }
        }


        public void CheckArticleData(Article dataToCheck)
        {
            if ((dataToCheck.Name != null) && (dataToCheck.Name.Trim().Length < 4))
            {
                ModelState.AddModelError("Name", "Der Name des Artikels muss sinnvoll sein!");
            }
            if (dataToCheck.Price < 0)
            {
                ModelState.AddModelError("Price", "Der Preis kann nicht negativ sein");
            }
            if(dataToCheck.Amount <= 0)
            {
                ModelState.AddModelError("Amount", "Die Menge kann nicht kleiner als 1 sein");
            }
            
        }
    }
}