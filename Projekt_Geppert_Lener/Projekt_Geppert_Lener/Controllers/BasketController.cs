﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using Projekt_Geppert_Lener.Models;
using Projekt_Geppert_Lener.Models.Database;

namespace Projekt_Geppert_Lener.Controllers
{
    public class BasketController : Controller
    {
        RepBasket repBasket;
        RepArticle repArticle;


        // GET: Basket
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BasketOverview()
        {
            List<BasketItem> bi = new List<BasketItem>();
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                try
                {
                    repBasket = new RepBasket();
                    repBasket.Open();
                    bi = repBasket.GetAllItemsAndArticles(u.ID);
                    Session["basket"] = bi;
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repBasket.Close();
                }
                return View(bi);
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        public ActionResult Bill()
        {
            Bill bill = new Bill();
            List<BasketItem> bi = (List<BasketItem>)Session["basket"];
            User u = (User)Session["loggedInUser"];
            if ((u != null)&&(bi != null))
            {
                if (bi.Count > 0)
                {
                    bill.BasketContent = bi;
                    bill.Buyer = u;
                    return View(bill);
                }
                else
                {
                    return View("Message", new Message("Warenkorb", "Ihr Warenkorb ist leer."));
                }
            }
            else if(u == null)
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
            else
            {
                return View("Message", new Message("Warenkorb", "Ihr Warenkorb konnte nicht gefunden werden."));
            }
        }

        public ActionResult Mail()
        {
            Bill bill = new Bill();
            List<BasketItem> bi = (List<BasketItem>)Session["basket"];
            User u = (User)Session["loggedInUser"];
            if ((u != null) && (bi != null))
            {
                if (bi.Count > 0)
                {
                    bill.BasketContent = bi;
                    bill.Buyer = u;
                    
                    //Send Mail
                    var mail = new MailMaker()
                    {
                        Absender = "ttt@web.de",
                        Empfänger = new List<string>() { bill.Buyer.EMail },
                        Betreff = "Danke für deinen Einkauf bei Tuiflshop",
                        Nachricht = bill.ToString(),
                        Servername = "smtp.web.de",
                        Port = "25",
                        Username = "Username",
                        Passwort = "Password",
                    };

                    mail.Send();

                    //basket entleeren
                    bill.Buyer.Basket.Clear();
                    Session["basket"] = bill.Buyer.Basket;
                    try
                    {
                        repBasket = new RepBasket();
                        repBasket.Open();
                        repBasket.ClearBasket(bill.Buyer.ID);
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repBasket.Close();
                    }

                    return View("Message", new Message("Bestellung eingegangen", "Vielen Dank für Ihre Bestellung"));
                }
                else
                {
                    return View("Message", new Message("Warenkorb", "Ihr Warenkorb ist leer."));
                }
            }
            else if (u == null)
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
            else
            {
                return View("Message", new Message("Warenkorb", "Ihr Warenkorb konnte nicht gefunden werden."));
            }
        }

        public ActionResult AddItem(int ItemToChange, int changeValue)
        {
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                try
                {
                    repBasket = new RepBasket();
                    repArticle = new RepArticle();
                    repBasket.Open();
                    repArticle.Open();
                    Basket amount = repBasket.GetOneItem(u.ID, ItemToChange);
                    Article articleAmount = repArticle.GetOneArticle(ItemToChange);
                    if (amount != null)
                    {
                        repArticle.ChangeAmount(ItemToChange, articleAmount.Amount - changeValue);
                        repBasket.AddArticleToBasket(ItemToChange, u.ID, amount.Amount + changeValue);
                    }
                    else
                    {
                        repArticle.ChangeAmount(ItemToChange, articleAmount.Amount - changeValue);
                        repBasket.AddArticleToBasket(ItemToChange, u.ID, changeValue);
                    }
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repBasket.Close();
                    repArticle.Close();
                }
                return RedirectToAction("BasketOverview");
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }
}
}