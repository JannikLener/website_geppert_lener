﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Projekt_Geppert_Lener.Models;
using Projekt_Geppert_Lener.Models.Database;
using MySql.Data.MySqlClient;

namespace Projekt_Geppert_Lener.Controllers
{
    public class UserController : Controller
    {

        RepUser repUser;
        RepBasket repBasket;

        // GET: User
        public ActionResult Index()
        {
            User u;
            if(Session["loggedInUser"] != null)
            {
                u = (User)Session["loggedInUser"];
            }
            else
            {
                u = null;
            }
            return View(u);
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (Session["loggedInUser"] == null)
            {
                User us = new User();
                return View(us);
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind bereits eingelogged."));
            }
        }

        [HttpPost]
        public ActionResult Login(User userDataFromForm)
        {
            repUser = new RepUser();
            repBasket = new RepBasket();
            User loggedInUser = null;
            List<BasketItem> basket = new List<BasketItem>();
            if (Session["loggedInUser"] == null)
            {
                if (userDataFromForm != null)
                {
                    try
                    {
                        repUser.Open();
                        repBasket.Open();
                        loggedInUser = repUser.Authenticate(userDataFromForm.Username, userDataFromForm.Password);                        
                        if (loggedInUser == null)
                        {
                            ModelState.AddModelError("Anmeldung", "Username / EMail oder Passwort falsch");
                        }
                        if (ModelState.IsValid)
                        {
                            basket = repBasket.GetAllItemsAndArticles(loggedInUser.ID);
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repUser.Close();
                        repBasket.Close();
                    }
                    if (ModelState.IsValid)
                    {
                        Session["loggedInUser"] = loggedInUser;
                        Session["basket"] = basket;
                        if(loggedInUser.IsAdmin == true)
                        {
                            Session["isAdmin"] = true;
                        }
                        else
                        {
                            Session["isAdmin"] = false;
                        }
                        return View("Message", new Message("Login", "Erfolgreich eingeloggt"));                
                    }
                    else
                    {
                        return View(userDataFromForm);
                    }
                }
                else
                {
                    return RedirectToAction("Login");
                }
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind bereits eingelogged."));
            }
        }

        [HttpGet]
        public ActionResult Registrierung()
        {
            if (Session["loggedInUser"] == null)
            {
                User us = new User();
                return View(us);
            }
            else
            {
                return View("Message", new Message("Registrierung", "Sie sind bereits eingelogged."));
            }
        }

        [HttpPost]
        public ActionResult Registrierung(User userDataFromForm)
        {
            repUser = new RepUser();
            repBasket = new RepBasket();
            if (Session["loggedInUser"]  == null)
            {
                if (userDataFromForm != null)
                {
                    CheckRegisterValues(userDataFromForm);
                    try
                    {
                        repUser.Open();
                        repBasket.Open();
                        if (ModelState.IsValid)
                        {
                            repUser.Register(userDataFromForm);
                            //User wird hier automatisch eingelogged
                            Session["loggedInUser"] = userDataFromForm;
                            Session["basket"] = repBasket.GetAllItemsAndArticles(userDataFromForm.ID);
                            if (userDataFromForm.IsAdmin == true)
                            {
                                Session["isAdmin"] = true;
                            }
                            else
                            {
                                Session["isAdmin"] = false;
                            }
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repUser.Close();
                        repBasket.Close();
                    }
                    if (ModelState.IsValid)
                    {
                        return View("Message", new Message("Registrierung", "Erfolgreich registriert"));
                    }
                    else
                    {
                        return View(userDataFromForm);
                    }
                }
                else
                {
                    return RedirectToAction("Registrierung");
                }
            }
            else
            {
                return View("Message", new Message("Registrierung", "Sie sind bereits eingelogged."));
            }
        }

        [HttpGet]
        public ActionResult ChangeData()
        {
            User u;
            if (Session["loggedInUser"] != null)
            {
                u = (User)Session["loggedInUser"];
            }
            else
            {
                u = null;
            }
            return View(u);
        }

        [HttpPost]
        public ActionResult ChangeData(User userDataFromForm)
        {
            repUser = new RepUser();
            bool noFail = true;
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                if (userDataFromForm != null)
                {
                    try
                    {
                        repUser.Open();
                        if (ModelState.IsValid)
                        {
                            User idUser = (User)Session["loggedInUser"];
                            userDataFromForm.ID = idUser.ID;
                            noFail = repUser.ChangeUser(userDataFromForm.ID, userDataFromForm);
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemiener Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repUser.Close();
                    }
                    if ((ModelState.IsValid)&&(noFail==true))
                    {
                        Session["loggedInUser"] = userDataFromForm;
                        return View("Message", new Message("Verändert", "Änderungen wurden durchgeführt"));
                    }
                    else
                    {
                        return View(userDataFromForm);
                    }
                }
                else
                {
                    return RedirectToAction("ChangeData");
                }
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        public ActionResult Logout()
        {
            Session["loggedInUser"] = null;
            Session["isAdmin"] = false;
            Session["basket"] = null;
            return View("Message", new Message("Logout", "Erfolgreich abgemeldet"));
        }

        public void CheckRegisterValues(User dataToCheck)
        {
            if ((dataToCheck.Firstname != null) && (dataToCheck.Firstname.Trim().Length < 3))
            {
                ModelState.AddModelError("Firstname", "Der Vorname muss mindestens 3 Zeichen lang sein");
            }
            if ((dataToCheck.Lastname == null) || (dataToCheck.Lastname.Trim().Length < 3))
            {
                ModelState.AddModelError("Lastname", "Der Nachname ist ein Pflichtfeld und muss mindestens 3 Zeichen lang sein");
            }
            if (dataToCheck.EMail == null)
            {
                ModelState.AddModelError("EMail", "EMail ist ein Pflichtfeld");
            }
            if ((dataToCheck.Username == null) || (dataToCheck.Username.Trim().Length < 3))
            {
                ModelState.AddModelError("Username", "Der Benutzername ist ein Pflichtfeld und muss mindestens 3 Zeichen lang sein");
            }
            if ((dataToCheck.Password == null) || (dataToCheck.Password.Length < 8) || (dataToCheck.Password.IndexOfAny(new[] { '!', '#', '%', '&', '/', '§' }) == -1))
            {
                ModelState.AddModelError("Password", "Das Passwort ist ein Pflichtfeld und muss mindestens 8 Zeichen lang sein und ein Sonderzeichen beinhalten");
            }
        }
    }
}