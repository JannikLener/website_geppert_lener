﻿using MySql.Data.MySqlClient;
using Projekt_Geppert_Lener.Models;
using Projekt_Geppert_Lener.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Projekt_Geppert_Lener.Controllers
{
    public class MarketController : Controller
    {
        RepArticle repArticle = new RepArticle();
        // GET: market
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MarketOverview()
        {
            List<Article> a;
            try
            {
                repArticle.Open();
                a = repArticle.GetAllArticles();
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
            }
            catch (Exception)
            {
                return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
            }
            finally
            {
                repArticle.Close();
            }
            if (a != null)
            {
                a = GetUnofficial(a);
            }
            else
            {
                a = null;
            }
            return View(a);
        }

        public ActionResult MarketDetailed(int id)
        {
            Article a;
            try
            {
                repArticle.Open();
                a = repArticle.GetOneArticle(id);
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
            }
            catch (Exception)
            {
                return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
            }
            finally
            {
                repArticle.Close();
            }
            return View(a);
        }

        [HttpGet]
        public ActionResult AddArticleToMarket()
        {
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                Article ar = new Article();
                ar.IsOfficial = false;
                return View(ar);
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        [HttpPost]
        public ActionResult AddArticleToMarket(Article articleDataFromForm)
        {
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                articleDataFromForm.Seller = u.Username;
                articleDataFromForm.IsOfficial = false;
                if (articleDataFromForm != null)
                {
                    try
                    {
                        repArticle.Open();
                        if (ModelState.IsValid)
                        {
                            //Pfad für gespeicherte Datei am Server generieren
                            string path = Path.Combine(Server.MapPath("~/Media/shop/"), Path.GetFileName(articleDataFromForm.Img.FileName));

                            //Bild auf diesem Pfad speichern
                            articleDataFromForm.Img.SaveAs(path);
                            //Pfad in Objekt setzen, damit dieser in der Datenbank verfügbar ist
                            articleDataFromForm.ImagePath = "/Media/shop/" + Path.GetFileName(articleDataFromForm.Img.FileName);
                            repArticle.AddArticle(articleDataFromForm);
                            ViewBag.Message = "File uploaded successfully";
                            repArticle.AddArticle(articleDataFromForm);
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemiener Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repArticle.Close();
                    }
                    if (ModelState.IsValid)
                    {
                        return View("Message", new Message("Artikel hinzufügen", "Erfolgreich hinzugefügt"));
                    }
                    else
                    {
                        return View(articleDataFromForm);
                    }
                }
                else
                {
                    return RedirectToAction("AddArticle");
                }
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        public List<Article> GetUnofficial(List<Article> allArticles)
        {
            List<Article> officialArticles = new List<Article>();
            foreach (Article a in allArticles)
            {
                if (a.IsOfficial == false)
                {
                    officialArticles.Add(a);
                }
            }
            if (officialArticles.Count > 0)
            {
                return officialArticles;
            }
            else
            {
                return null;
            }
        }

        public ActionResult sendMessage(string seller)
        {
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                if(seller != null)
                {
                    Session["Seller"] = seller;
                    return RedirectToAction("newMessage","Chat");
                }
                else
                {
                    return View("Message", new Message("Error", "Verkäufer nicht gefunden."));
                }
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }
    }
}