﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Projekt_Geppert_Lener.Models.Database;
using Projekt_Geppert_Lener.Models;
using MySql.Data.MySqlClient;

namespace Projekt_Geppert_Lener.Controllers
{
    public class ChatController : Controller
    {
        RepChat repChat = new RepChat();
        // GET: Chat
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllMessagesToMe()
        {
            List<ChatUser> cu = new List<ChatUser>();
            List<Chat> c;
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                try
                {
                    repChat.Open();
                    c = repChat.AllMessagesToMe(u.ID);
                    if (c != null)
                    {
                        foreach (Chat cc in c)
                        {
                            cu.Add(new ChatUser(cc, repChat.IDToName(cc.Sender)));
                        }
                    }                
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repChat.Close();
                }
                if (cu.Count == 0)
                {
                    cu = null;
                }
                return View(cu);
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        public ActionResult AllMessagesfromMe()
        {
            List<ChatUser> cu = new List<ChatUser>();
            List<Chat> c;
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                try
                {
                    repChat.Open();
                    c = repChat.AllMessagesFromMe(u.ID);
                    if (c != null)
                    {
                        foreach (Chat cc in c)
                        {
                            cu.Add(new ChatUser(cc, repChat.IDToName(cc.Sender)));
                        }
                    }
                }
                catch (MySqlException)
                {
                    return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                }
                catch (Exception)
                {
                    return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                }
                finally
                {
                    repChat.Close();
                }
                if (cu.Count == 0)
                {
                    cu = null;
                }
                return View(cu);
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        [HttpGet]
        public ActionResult newMessage()
        {
            User u = (User)Session["loggedInUser"];
            if (u != null)
            {
                ChatUser cu = new ChatUser();
                if (Session["seller"] != null)
                {
                    cu.RecieverName = (string)Session["seller"];
                    Session["seller"] = null;
                }
                return View(cu);
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
        }

        [HttpPost]
        public ActionResult newMessage(ChatUser dataFromForm)
        {
            User u = (User)Session["loggedInUser"];
            dataFromForm.Chat.Sender = u.ID;
            if (u != null)
            {
                if (dataFromForm != null)
                {
                    try
                    {
                        repChat.Open();
                        dataFromForm.Chat.Reciever = repChat.NameToID(dataFromForm.RecieverName);
                        if (dataFromForm.Chat.Reciever == -1)
                        {
                            ModelState.AddModelError("Empfänger", "Name nicht gefunden");
                        }
                        if (ModelState.IsValid)
                        {
                            repChat.NewMessage(dataFromForm.Chat);
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit ein Problem mit der Datenbank"));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Ein allgemeiner Fehler ist aufgetreten"));
                    }
                    finally
                    {
                        repChat.Close();
                    }
                    if (ModelState.IsValid)
                    {
                        return View("Message", new Message("Nachricht versenden", "Nachricht wurde erfolgreich versendet"));
                    }
                    else
                    {
                        return View(dataFromForm);
                    }
                }
                else
                {
                    return RedirectToAction("newMessage");
                }
            }
            else
            {
                return View("Message", new Message("Login", "Sie sind nicht eingelogged."));
            }
}
    }
}